class AddTopicStatsToCategoryTable < ActiveRecord::Migration[5.1]
  def change
    add_column :categories, :topic_count, :integer
    add_column :categories, :topic_retry, :boolean
  end
end
