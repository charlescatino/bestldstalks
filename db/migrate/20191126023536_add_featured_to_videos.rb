class AddFeaturedToVideos < ActiveRecord::Migration[5.1]
  def up
    add_column :videos, :featured, :boolean, null: false, default: false
  end

  def down
  	remove_column :videos, :featured
  end
end
