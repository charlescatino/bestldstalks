class AddGenderToUsers < ActiveRecord::Migration[5.1]
  def change
  	add_column :users, :gender, :integer

    User.reset_column_information
    User.update_all(gender: 2)
  end
end
