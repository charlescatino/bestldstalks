class CreateRatings < ActiveRecord::Migration[5.1]
  def change
    create_table :ratings do |t|
      t.integer :user_id
      t.integer :video_id
      t.integer :value
      t.timestamps
    end

    add_index :ratings, :user_id
    add_index :ratings, :video_id
  end
end