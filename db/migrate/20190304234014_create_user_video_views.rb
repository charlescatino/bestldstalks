class CreateUserVideoViews < ActiveRecord::Migration[5.1]
  def change
    create_table :user_video_views do |t|
    	t.integer :user_id
    	t.integer :video_id
    	t.boolean :emailed
    	t.timestamps
    end

    add_index :user_video_views, :user_id
    add_index :user_video_views, :video_id
  end
end
