class AddChurchAffiliationToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :church_affiliation, :integer
  end
end
