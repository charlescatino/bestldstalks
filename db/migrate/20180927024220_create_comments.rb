class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
      t.references :video, foreign_key: true
      t.references :parent, index: true
      t.text :body

      t.timestamps null: false
    end
  end
end
