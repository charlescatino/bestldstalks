class SetDatabaseFieldsNotNull < ActiveRecord::Migration[5.1]
  def change
  	change_column :categories, :name, :string, null: false

  	change_column :comments, :body, :text, null: false

  	change_column :ratings, :user_id, :integer, null: false
  	change_column :ratings, :video_id, :integer, null: false
  	change_column :ratings, :value, :integer, null: false

  	change_column :user_video_views, :user_id, :integer, null: false
  	change_column :user_video_views, :video_id, :integer, null: false
  	change_column :user_video_views, :emailed, :boolean, null: false

  	change_column :users, :first_name, :string, null: false
  	change_column :users, :first_name, :string, null: false
  	change_column :users, :zip, :string, null: false
  	change_column :users, :gender, :string, null: false
  	change_column :users, :dob, :date, null: false

  	change_column :videos, :foreign_id, :integer, null: false
  	change_column :videos, :friendly_url, :string, null: false
  	change_column :videos, :title, :string, null: false
  	change_column :videos, :url, :string, null: false
  	change_column :videos, :active, :boolean, null: false
  end
end
