class CreateVideos < ActiveRecord::Migration[5.1]
  def change
    create_table :videos do |t|
      t.integer :foreign_id
      t.string :friendly_url
      t.string :title
      t.string :author
      t.text :transcript
      t.integer :duration
      t.string :image
      t.string :description

      t.timestamps
    end
  end
end
