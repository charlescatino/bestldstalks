class AddActiveToVideos < ActiveRecord::Migration[5.1]
  def up
    add_column :videos, :active, :boolean

    Video.reset_column_information
    Video.update_all(active: true)
  end

  def down
  	remove_column :videos, :active
  end
end
