class SwitchVideoForeignIdToBigInt < ActiveRecord::Migration[5.1]
  def change
  	change_column :videos, :foreign_id, :bigint
  end
end
