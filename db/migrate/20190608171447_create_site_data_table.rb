class CreateSiteDataTable < ActiveRecord::Migration[5.1]
  def change
    create_table :site_data do |t|
      t.text :about_us, null: false

      t.timestamps
    end
  end
end
