# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20191222033234) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "categories", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "topic_count"
    t.boolean "topic_retry"
  end

  create_table "categories_videos", id: false, force: :cascade do |t|
    t.bigint "video_id", null: false
    t.bigint "category_id", null: false
    t.index ["category_id", "video_id"], name: "index_categories_videos_on_category_id_and_video_id"
    t.index ["video_id", "category_id"], name: "index_categories_videos_on_video_id_and_category_id"
  end

  create_table "comments", force: :cascade do |t|
    t.bigint "video_id"
    t.bigint "parent_id"
    t.text "body", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.index ["parent_id"], name: "index_comments_on_parent_id"
    t.index ["user_id"], name: "index_comments_on_user_id"
    t.index ["video_id"], name: "index_comments_on_video_id"
  end

  create_table "ratings", force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "video_id", null: false
    t.integer "value", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_ratings_on_user_id"
    t.index ["video_id"], name: "index_ratings_on_video_id"
  end

  create_table "site_data", force: :cascade do |t|
    t.text "about_us", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end
  create_table "user_video_views", force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "video_id", null: false
    t.boolean "emailed", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_video_views_on_user_id"
    t.index ["video_id"], name: "index_user_video_views_on_video_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "first_name", null: false
    t.string "last_name"
    t.date "dob", null: false
    t.string "zip", null: false
    t.integer "church_affiliation"
    t.boolean "admin"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "gender", null: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "videos", force: :cascade do |t|
    t.bigint "foreign_id", null: false
    t.string "friendly_url", null: false
    t.string "title", null: false
    t.string "author"
    t.text "transcript"
    t.integer "duration"
    t.string "image"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "url", null: false
    t.datetime "date"
    t.boolean "active", null: false
    t.boolean "featured", default: false, null: false
  end

  add_foreign_key "comments", "users"
  add_foreign_key "comments", "videos"
end
