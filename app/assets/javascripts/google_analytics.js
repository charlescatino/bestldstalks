document.addEventListener('turbolinks:load', function(event) {
  if (typeof gtag === 'function') {
    gtag('config', 'UA-134224832-1', {
      'page_location': event.data.url
    })
  }
})