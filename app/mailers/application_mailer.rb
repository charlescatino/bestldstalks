class ApplicationMailer < ActionMailer::Base
  default from: ENV['GODADDY_USERNAME']
  layout 'mailer'
end
