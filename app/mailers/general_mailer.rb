class GeneralMailer < ApplicationMailer

  def contact_email
    @name = params[:name]
    @email = params[:email]
    @message = params[:message]
    mail(to: ENV['GODADDY_USERNAME'], subject: "Contact form filled out by #{@name}")
  end

  def process_lds_conference_results
  	@url = params[:url]
  	@newVideos = params[:newVideos]
  	@updatedVideos = params[:updatedVideos]
  	@skippedVideos = params[:skippedVideos]
  	@failedVideos = params[:failedVideos]
  	mail(to: params[:email], subject: "Finished processing Conference #{@url}")
  end

  def process_topics_results
    @failedTopics = params[:failedTopics]
    @successfulTopics = params[:successfulTopics]
    mail(to: params[:email], subject: "Finished processing general conference topics")
  end
end