class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :dob, :zip, :church_affiliation, :gender])
    devise_parameter_sanitizer.permit(:account_update, keys: [:first_name, :last_name, :dob, :zip, :church_affiliation, :gender])
  end

  def load_filter_inputs_data
  	@ddl_rating_options = [["4+ stars", "4"], ["3+ stars", "3"], ["2+ stars", "2"], ["1+ stars", "1"]]
  	@ddl_length_options = [["< 5 minutes", "5"], ["< 10 minutes", "20"], ["< 15 minutes", "15"], ["< 20 minutes", "20"], ["20+ minutes", "600"]]
  	@ddl_date_options = [["< 1 month", "1"], ["< 6 months", "6"], ["< 1 year", "12"], ["< 2 years", "24"], ["2+ years", "600"]]
    @categories = Category.all.order(:name)
  end
end
