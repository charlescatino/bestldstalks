class AdminController < ApplicationController
  before_action :authenticate_user!
  before_action :ensure_admin

  def index
    @video_count_active = Video.count()
    @videos_inactive = Video.include_inactive.where(active: false)
    @videos_featured = Video.where(featured: true)

    @user_count = User.count()
    @comment_count = Comment.count()
    @rating_count = Rating.count()

    @site_data = SiteData.first
  end

  def crawl_page
  	@results = {:url => params[:url], :errors => []}

  	if params[:url].blank?
  	  @results[:errors] << "Error: You must provide a URL"
  	end

  	case params[:page_type]
  	when "conference"
      ProcessConferenceJob.perform_later(params[:url], current_user.email)
    else
      @results[:errors] << "Error: unknown page type"
    end

    respond_to do |format|
      format.js
    end
  end

  def crawl_categories
    ProcessCategoriesJob.perform_later(current_user.email)
  end

  def update_about
    @results = {:url => params[:url], :errors => []}
    if params[:about].blank?
      @results[:errors] << "Error: No content provided"
    end

    site_data = SiteData.first
    site_data.about_us = params[:about]
    site_data.save

    respond_to do |format|
      format.js
    end
  end

  private

  def ensure_admin
    unless current_user.admin?
      redirect_to root_path
    end
  end
end
