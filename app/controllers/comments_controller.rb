class CommentsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_video, only: [:create]
  before_action :set_comment, only: [:destroy]

  # POST /comments
  # POST /comments.json
  def create
    @comment = @video.comments.new(comment_params)
    @new_comment = Comment.new

    respond_to do |format|
      if @comment.save
        if @comment.is_sub_comment?
          format.js { render "create_sub" }
        else
          format.js
        end
      else
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # # PATCH/PUT /comments/1
  # # PATCH/PUT /comments/1.json
  # def update
  #   respond_to do |format|
  #     if @comment.update(comment_params)
  #       format.html { redirect_to @comment, notice: 'Comment was successfully updated.' }
  #       format.json { render :show, status: :ok, location: @comment }
  #     else
  #       format.html { render :edit }
  #       format.json { render json: @comment.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    if @comment.user_id == current_user.id || current_user.admin?
      @comment.destroy
    end
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
  
  # Use callbacks to share common setup or constraints between actions.
  def set_video
    @video = Video.include_inactive.find(params[:video_id])
  end

  def set_comment
    @comment = Comment.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def comment_params
    params.require(:comment).permit(:body, :user_id, :parent_id)
  end
end
