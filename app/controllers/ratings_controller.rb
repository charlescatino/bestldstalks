class RatingsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_video
  before_action :set_rating, only: [:show, :edit, :update, :destroy]

  # POST /ratings
  # POST /ratings.json
  def create
    @rating = @video.ratings.new(rating_params)

    respond_to do |format|
      if @rating.save
        format.js
      else
        format.json { render json: @rating.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ratings/1
  # PATCH/PUT /ratings/1.json
  def update
    respond_to do |format|
      if @rating.update(rating_params)
      	format.js { render 'create.js.erb' }
      else
        format.json { render json: @rating.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_video
    @video = Video.include_inactive.find(params[:video_id])
  end
  
  def set_rating
    @rating = @video.ratings.find(params[:id])
  end
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def rating_params
    params.require(:rating).permit(:value, :user_id)
  end
end