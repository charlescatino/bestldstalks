class HomeController < ApplicationController
  skip_before_action :verify_authenticity_token, :only => [:send_contact]
  before_action :get_about_us_variables, only: [:about, :send_contact]
  def home
  	@featured_talks = Video.where(featured: true).sample(4)
  	
    @recent_talks = Video.where(date: Video.maximum("date")).sample(4)
    # There should be more than 4 videos matching latest date since they come in waves from conference,
    # but just in case.....
    if @recent_talks.count() != 4
      @recent_talks = Video.order(date: :desc).take(20).sample(4)
    end

    # TODO: move to scope somehow?
  	@popular_talks = Video.left_outer_joins(:ratings).where.not(ratings: {id: nil}).distinct
      .select{ |vid| vid.average_rating > 3.0}.sort_by{ |vid| vid.average_rating}.reverse.first(4)

    @video_filter = VideoFilter.new
    @video_filter.clear
    load_filter_inputs_data
  end

  # GET
  def about
  end

  # POST (contact form listener)
  def send_contact
    GeneralMailer.with(
      name: params[:name],
      email: params[:email],
      message: params[:message]).contact_email.deliver_later
    
    flash.now[:notice] = 'Your message has been received.'
    render "about"
  end

  private

  def get_about_us_variables
    @name = nil
    @email = nil
    if user_signed_in?
      @name = current_user.full_name
      @email = current_user.email
    end
    @about_us_text = SiteData.first.about_us
  end
end