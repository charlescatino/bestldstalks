class VideosController < ApplicationController
  skip_before_action :verify_authenticity_token, :only => [:user_click]

  def index
    @video_filter = VideoFilter.new(params)

    @videos = Video.where(nil)
    if @video_filter.any_filters?
      unless @video_filter.author.blank?
        @videos = @videos.author_like(@video_filter.author)
      end
      unless @video_filter.title.blank?
        @videos = @videos.title_like(@video_filter.title)
      end
      unless @video_filter.topic.blank?
      	@videos = @videos.has_topic(@video_filter.topic)
      end
      unless @video_filter.date == 0
        @videos = @videos.greater_than_date(DateTime.now << @video_filter.date)
      end
      unless @video_filter.rating.blank?
        # TODO: move to scope somehow?
        @videos = @videos.select { |vid| vid.ratings.any? and vid.average_rating >= @video_filter.rating.to_d }
      end

      # TODO: these below once we have those stats on the video...
      # TODO: length/duration
      # TODO: date/age
      @videos = @videos.order(date: :desc)
    else
      @videos = Video.all.sample(40)
    end

    
    load_filter_inputs_data
  end

  def show
    # TODO: show video not found if user isn't admin? So people can't save URLs?
    @video = Video.include_inactive.find(params[:id])
    @related_videos = @video.related_videos
    @new_comment = Comment.new

    @rating = Rating.new
    if user_signed_in?
      user_rating = @video.ratings.find_by user_id: current_user.id
      if user_rating
        @rating = user_rating
      end
    end
  end

  def set_active_status
    @video = Video.include_inactive.find(params[:id])
    logger.info "Setting video #{@video.id} active flag to #{params[:featured]}"
    @video.active = params[:active]
    unless @video.active?
      logger.info "Setting video #{@video.id} featured flag to false, since video is being set inactive"
      @video.featured = false
    end
    @video.save

    # TODO: return view or success or something
  end

  def set_featured_status
    @video = Video.find(params[:id])
    unless @video.nil?
      logger.info "Setting video #{@video.id} featured flag to #{params[:featured]}"
      @video.featured = params[:featured]
      @video.save
    else
      # video not found or inactive - unable to set as featured
    end

    # TODO: return view or success or something
  end

  def user_click
    if user_signed_in?
      UserVideoView.create({:user_id => current_user.id, :video_id => params[:id], :emailed => false})
    end
    head :no_content
  end
end