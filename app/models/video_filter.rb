class VideoFilter
  @@rating = nil
  @@author = nil
  @@date = nil
  @@topic = nil
  @@length = nil
  @@title = nil

  def initialize(params = nil)
    unless params.nil?
      @@rating = params["rating"]
      @@author = params["author"]
      
      begin
        @@date = params["date"].to_i
      rescue
        @@date = nil
      end

      @@topic = params["topic"]
      @@length = params["length"]
      @@title = params["title"]
    end
  end

  def any_filters?
    return !@@rating.blank? || !@@author.blank? || !(@@date == 0) || !@@topic.blank? || !@@length.blank? || !@@title.blank?
  end

  def clear
    @@rating = nil
    @@author = nil
    @@date = nil
    @@topic = nil
    @@length = nil
    @@title = nil
  end

  def rating
    @@rating
  end

  def author
    @@author
  end

  def date
    @@date
  end

  def topic
    @@topic
  end

  def length
    @@length
  end

  def title
    @@title
  end

end