class Comment < ApplicationRecord
  belongs_to :video
  belongs_to :user

  # belongs_to :parent, class_name: "Comment", foreign_key: "parent_id"
  has_many :sub_comments, -> (me) { where(parent_id: me.id).order("created_at asc") }, class_name: "Comment", foreign_key: "parent_id", dependent: :delete_all

  def is_sub_comment?
  	return self.parent_id != nil
  end
end