class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  enum church_affiliation: [:member, :non_member, :other]
  enum gender: [:male, :female, :neutral]

  validates :first_name, :presence => true
  validates :last_name, :presence => true
  validates :dob, :presence => true
  validates :zip, :presence => true

  has_many :comments
  has_many :ratings

  def public_name
  	return self.first_name.titleize + " " + self.last_name[0,1].upcase + "."
  end

  def full_name
    return self.first_name.titleize + " " + self.last_name.titleize
  end

  def self.church_affiliation_ddl_options
    return self.church_affiliations.map {|k, v| [k.humanize.capitalize, k]}
  end

  def self.gender_ddl_options
    return [["Male", "male"], ["Female", "female"], ["Other / Prefer not to answer", "neutral"]]
  end
end