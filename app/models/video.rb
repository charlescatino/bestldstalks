class Video < ApplicationRecord
  has_and_belongs_to_many :categories
  has_many :comments, -> { where(parent_id: nil).order("created_at desc") }
  has_many :ratings
  has_many :user_video_views
  
  default_scope { where(active: true) }
  scope :title_like, -> (title) { where("lower(title) like ?", "%#{title.downcase}%") }
  scope :author_like, -> (author) { where("lower(author) like ?", "%#{author.downcase}%") }
  scope :has_topic, -> (topic_id) { joins(:categories).where("categories.id = ?", topic_id) }
  scope :greater_than_date, -> (date) { where("date > ?", date)}

  def related_videos
    if self.categories.any?
  	  return Video.has_topic(self.categories.sample.id).sample(4)
    end
    return Video.all.sample(4)
  end

  def average_rating
    if self.ratings.any?
      return self.ratings.average(:value)
    end
    return nil
  end

  def self.include_inactive
    unscoped
  end

  def date_friendly
    return self.date.strftime("%B %Y")
  end
end