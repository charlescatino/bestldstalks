class ProcessCategoriesJob < ApplicationJob
  queue_as :default

  @@url = 'https://www.churchofjesuschrist.org'
  @@categories

  # Church of Jesus Christ calls these "topics"
  # We call them "categories"

  def perform(email)
  	require 'openssl'
    require 'open-uri'
    require 'ostruct'
    topics_url = @@url + '/general-conference/topics'
    doc = Nokogiri::HTML(open(topics_url, :ssl_verify_mode => OpenSSL::SSL::VERIFY_NONE))

    @failedTopics = Array.new
    @topics = Array.new
    @@categories = Category.all

    doc.css('.lumen-tile__title').each_with_index do |topic_link_div, i|
      begin
        topic = {}
        chunks = topic_link_div.text.strip.split("(") 
        topic[:name] = chunks[0].strip.downcase
        topic[:count] = chunks[1].nil? ? 0 : chunks[1].split(")")[0].strip.to_i
        url = topic_link_div.css('a')[0]['href']

        category = @@categories.find_by_name(topic[:name])
        if (category and category.topic_count == topic[:count] and category.topic_retry != true)
          logger.info "Skipping topic='#{topic[:name]}' because we have latest with count=#{category.topic_count}"
          next
        end

        video_urls = []
        topicDoc = Nokogiri::HTML(open(@@url + url, :ssl_verify_mode => OpenSSL::SSL::VERIFY_NONE))
        logger.info "checking video page for topic='#{topic[:name]}'; Using URL=#{@@url + url}"

        topicDoc.css('.lumen-tile__text-wrapper').each_with_index do |video_text_div, j|
          video_year = video_text_div.css('.lumen-tile__metadata').text.strip.split(" ")[1].to_i
          if (video_year > (Date.today.year-1))
            video_url = video_text_div.css('a')[0]['href'].split("?").first
            logger.info "Adding video to category list. Year=#{video_year} Url=#{video_url}"
            video_urls.push(video_url)
          end
        end

        topic[:category_save_results] = update_video_categories(topic[:name], video_urls, topic[:count])
        @topics << topic
      rescue => ex
        logger.error ex.message
        logger.error ex.backtrace
        @failedTopics << topic
      end
      sleep(1)

      #TODO: comment out
      # if i > 5
      #   logger.info "STOPPING EARLY"
      #   break
      # end

    end

    #TODO: save @topics counts to db

    logger.info "DONE"
    logger.info "Failed topics: #{@failedTopics.count}"
    logger.info "Successful topics: #{@topics}"

    GeneralMailer.with(
      email: email,
      failedTopics: @failedTopics,
      successfulTopics: @topics).process_topics_results.deliver_later
    logger.info "Results emailed to #{email}"
  end

  private

  def update_video_categories(category_name, video_urls, topic_count)
    is_new_category = false
    added = []
    failure = []

    logger.info "Attempting to associate category=#{category_name}"
    logger.info "With videos: #{video_urls}"
    category = @@categories.find_by_name(category_name)
    unless (category)
      category = Category.create(:name => category_name)
      logger.info "Creating new category name=#{category.name}"
      is_new_category = true
    end

    video_urls.each_with_index do |url, i|
      unless category.videos.where("url LIKE '%#{url}%'").any?
        video = Video.where("url LIKE '%#{url}%'").first
        if (video)
          video.categories << category
          video.save!
          logger.info "Added category=#{category.name} to video video_id=#{video.id}"
          added.push(video.id)
        else
          logger.error "Video not found with url=#{url}"
          failure.push(@@url + url)
        end
      else
        logger.info "Category=#{category.name} already associated with video_url=#{url}"
      end
    end

    category.topic_count = topic_count
    logger.info "failure size: #{failure.size}, count: #{topic_count}" # TODO: remove
    category.topic_retry = (failure.size > 0 or topic_count < 1)
    category.save!

    results = {}
    results[:new_associations_video_id] = added
    results[:failure_urls] = failure
    results[:category_added] = is_new_category
    return results
  end

end