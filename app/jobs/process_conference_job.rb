class ProcessConferenceJob < ApplicationJob
  queue_as :default

  # def perform(conference_url)
  #   require 'mechanize'
  #   agent = Mechanize.new

  #   page = agent.get(conference_url)

  #   videos = page.search("div.lumen-tile--list")
  #   logger.info videos
  # end

  @@base_url = 'https://www.churchofjesuschrist.org'

  def perform(conference_url, email)
  	require 'openssl'
    require 'open-uri'
    require 'ostruct'
    doc = Nokogiri::HTML(open(conference_url, :ssl_verify_mode => OpenSSL::SSL::VERIFY_NONE))

    @newVideos = 0
    @updatedVideos = 0
    @skippedVideos = 0
    @failedVideos = Array.new

    video_date_text = doc.css('h1.title')[0].text.strip.split[0, 2]
    video_date = (video_date_text[0] + ' ' + video_date_text[1]).to_date

    doc.css('.lumen-tile--list').each_with_index do |video_element, i|
      begin
        video_details = get_video_details(video_element)
        video_details[:date] = video_date
        logger.info video_details

        add_or_update_video(video_details)
      rescue => ex
        logger.error ex.message
        logger.error ex.backtrace
        begin
          @failedVideos << (@@base_url + video_element.css('a')[0]['href'])
        rescue => ex
          @failedVideos << "Failed to retrieve URL of video"
        end
      end

      # if i > 3
      #   logger.info "STOPPING EARLY"
      #   break
      # end

    end
    logger.info "DONE"
    logger.info "New videos: #{@newVideos}"
    logger.info "Updated videos: #{@updatedVideos}"
    logger.info "Skipped videos: #{@skippedVideos}"
    logger.info "Failed videos: #{@failedVideos.count}"

    GeneralMailer.with(
      url: conference_url,
      email: email,
      newVideos: @newVideos,
      updatedVideos: @updatedVideos,
      skippedVideos: @skippedVideos,
      failedVideos: @failedVideos).process_lds_conference_results.deliver_later
    logger.info "Results emailed to #{email}"
  end

  private

  def get_video_details(video)
    details = {}
    
    video_url_segment = video.css('a')[0]['href'].split("?").first
    logger.info "Processing details for video: #{video_url_segment}"

    details[:friendly_url] = video_url_segment.split("/").last
    details[:url] = @@base_url + video_url_segment

    if (video_url_segment.include? '/media/') # todo: put in override class?
      details[:title] = video.css('.lumen-tile__title').text.strip
    else
      details[:title] = video.css('.lumen-tile__title').css('div')[1].text.strip
    end

    details[:author] = video.css('.lumen-tile__content').text.strip

    videoDoc = Nokogiri::HTML(open(details[:url], :ssl_verify_mode => OpenSSL::SSL::VERIFY_NONE))
    details[:description] = videoDoc.css('.kicker').text.strip

    begin
      # video_element = videoDoc.css('video')
      # logger.info video_element
      # Appears to be second <video> element on page, but other one is in a #document tag?
      details[:image] = videoDoc.css('video')[0]['poster']
    rescue => ex
      logger.info "Unable to retrieve image for video: #{video_url_segment}"
    end
    

    if (video_url_segment.include? '/media/') # todo: put in override class?
      details[:foreign_id] = video_url_segment.split("/").last
    else
      details[:foreign_id] = videoDoc.css('article')[0]['data-aid']
      logger.info "article html component:"
      logger.info videoDoc.css('article')
    end

    # details[:date] = videoDoc.css('.sticky-banner__link').text.strip.to_date

    # TODO: no longer available. :(
    # Can reverse lookup using the topics page?
    # https://www.churchofjesuschrist.org/general-conference/topics?lang=eng
    categories = []
    # videoDoc.css('.related-items__topics').css('.drawerList').each do |category|
    #   categories.push(category['data-title'].downcase)
    # end
    # details[:categories] = categories

    # TODO: NEED TO STRIP links (footnotes), emphasis tags (em) and anything else.
    transcript = ""
    videoDoc.css('.body-block').css('p').each do |bodyChunk|
      transcript << (bodyChunk.text + "<br>")
    end
    details[:transcript] = transcript

    details[:duration] = 0

    # mechanize - need non-windows machine?
    # videoDoc.css('.lumen-media-block').css('a').click
    # duration = videoDoc.css('.vjs-duration-display').text.strip
    return details
  end

  def add_or_update_video(video_details)
    categories = video_details[:categories]
    video_details.delete(:categories)

    video = Video.include_inactive.find_by_foreign_id(video_details[:foreign_id])
    if (video) # update
      video.assign_attributes(video_details)        
      if video.changed?
        logger.info "updated video"
        video.save
        @updatedVideos += 1
      else
        logger.info "skipped updating video, nothing modified"
        @skippedVideos += 1
      end
    else # create
      newVid = Video.create(video_details)

      unless categories.nil?
        categories.each do |c|
          category = Category.find_by_name(c)
          if (category)
            newVid.categories << category
          else
            newVid.categories << Category.create(:name => c)
          end
          newVid.active = true
          newVid.save!
          logger.info "created new video"
        end
      end

      @newVideos += 1
    end
  end
end