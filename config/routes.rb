Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'home#home'

  resources :videos, :only => [:index, :show] do
    member do
      post "user_click"
      post "set_active_status"
      post "set_featured_status"
    end
  	resources :comments, only: [:create, :update]
  	resources :ratings, only: [:create, :update]
  end
  resources :comments, only: [:destroy]

  get "/about", to: "home#about"
  post "/about", to: "home#send_contact"

  get "/admin", to: "admin#index"
  post "/admin/crawl_page", to: "admin#crawl_page"
  post "/admin/update_about", to: "admin#update_about"
  post "/admin/crawl_categories", to: "admin#crawl_categories"
end
